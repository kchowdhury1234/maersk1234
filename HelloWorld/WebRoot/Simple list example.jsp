<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
<script type='text/javascript' src='knockout-2.2.1.js'>
  
  var SimpleListModel = function(items) {     this.items = ko.observableArray(items);     this.itemToAdd = ko.observable("");     this.addItem = function() {         if (this.itemToAdd() != "") {             this.items.push(this.itemToAdd()); // Adds the item. Writing to the "items" observableArray causes any associated UI to update.             this.itemToAdd(""); // Clears the text box, because it's bound to the "itemToAdd" observable         }     }.bind(this);  // Ensure that "this" is always this view model };  ko.applyBindings(new SimpleListModel(["Alpha", "Beta", "Gamma"])); 
  
  
  </script>
  
    <base href="<%=basePath%>">
    
    <title>My JSP 'Simple list example.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <form data-bind="submit: addItem">     New item:     <input data-bind='value: itemToAdd, valueUpdate: "afterkeydown"' />     <button type="submit" data-bind="enable: itemToAdd().length > 0">Add</button>     <p>Your items:</p>     <select multiple="multiple" width="50" data-bind="options: items"> </select> </form> 
 <br>
  </body>
</html>
