<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
<script type='text/javascript' src='knockout-2.2.1.js'>
  var GiftModel = function(gifts) {  
     var self = this;  
        self.gifts = ko.observableArray(gifts);   
            self.addGift = function() {       
              self.gifts.push({             name: "",             price: ""        });     };   
                  self.removeGift = function(gift) {         self.gifts.remove(gift);     };       self.save = function(form) {         alert("Could now transmit to server: " + ko.utils.stringifyJson(self.gifts));         // To actually transmit to server as a regular form post, write this: ko.utils.postJson($("form")[0], self.gifts);     }; };   var viewModel = new GiftModel([     { name: "Tall Hat", price: "39.95"},     { name: "Long Cloak", price: "120.00"} ]); ko.applyBindings(viewModel);   // Activate jQuery Validation $("form").validate({ submitHandler: viewModel.save }); 
  
  </script>
  
    <base href="<%=basePath%>">
    
    <title>My JSP 'Grid editor example.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <form action='/someServerSideHandler'>     <p>You have asked for <span data-bind='text: gifts().length'>&nbsp;</span> gift(s)</p>     <table data-bind='visible: gifts().length > 0'>         <thead>             <tr>                 <th>Gift name</th>                 <th>Price</th>                 <th />             </tr>         </thead>         <tbody data-bind='foreach: gifts'>             <tr>                 <td><input class='required' data-bind='value: name, uniqueName: true' /></td>                 <td><input class='required number' data-bind='value: price, uniqueName: true' /></td>                 <td><a href='#' data-bind='click: $root.removeGift'>Delete</a></td>             </tr>         </tbody>     </table>       <button data-bind='click: addGift'>Add Gift</button>     <button data-bind='enable: gifts().length > 0' type='submit'>Submit</button> </form> 
 <br>
  </body>
</html>
