<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  
<script type='text/javascript' src='knockout.js'>
 </script> 
 
    
    <title>My JSP 'MyJsp.jsp' starting page</title>



  </head>
  
  <body>
  <form data-bind="submit: addItem">  
       New item:   
         <input data-bind='value: itemToAdd, valueUpdate: "afterkeydown"' />     <button type="submit" data-bind="enable: itemToAdd().length > 0">Add</button>     <p>Your items:</p>     <select multiple="multiple" width="50" data-bind="options: items"> </select> </form> 
 <br>
 <br>
 
 <script>
var SimpleListModel = function additems(items) 
{     this.items = ko.observableArray(items);  
   this.itemToAdd = ko.observable("");   
     this.addItem = function additems() {       
       if (this.itemToAdd() != "") {         
           this.items.push(this.itemToAdd()); // Adds the item. Writing to the "items" observableArray causes any associated UI to update. 
    this.itemToAdd(""); // Clears the text box, because it's bound to the "itemToAdd" observable    
         }     }.bind(this); 
  // Ensure that "this" is always this view model
   };  ko.applyBindings(new SimpleListModel(["Alpha", "Beta", "Gamma"])); 
</script>
 
  </body>
</html>
