<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
<script type='text/javascript' src='knockout.js'>
  var initialData = [     { name: "Well-Travelled Kitten", sales: 352, price: 75.95 },     { name: "Speedy Coyote", sales: 89, price: 190.00 },     { name: "Furious Lizard", sales: 152, price: 25.00 },     { name: "Indifferent Monkey", sales: 1, price: 99.95 },     { name: "Brooding Dragon", sales: 0, price: 6350 },     { name: "Ingenious Tadpole", sales: 39450, price: 0.35 },     { name: "Optimistic Snail", sales: 420, price: 1.50 } ];  var PagedGridModel = function(items) {     this.items = ko.observableArray(items);      this.addItem = function() {         this.items.push({ name: "New item", sales: 0, price: 100 });     };      this.sortByName = function() {         this.items.sort(function(a, b) {             return a.name < b.name ? -1 : 1;         });     };      this.jumpToFirstPage = function() {         this.gridViewModel.currentPageIndex(0);     };      this.gridViewModel = new ko.simpleGrid.viewModel({         data: this.items,         columns: [             { headerText: "Item Name", rowText: "name" },             { headerText: "Sales Count", rowText: "sales" },             { headerText: "Price", rowText: function (item) { return "$" + item.price.toFixed(2) } }         ],         pageSize: 4     }); };  ko.applyBindings(new PagedGridModel(initialData)); 
  
  
  
  </script>
    <base href="<%=basePath%>">
    
    <title>My JSP 'Paged grid.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <div data-bind='simpleGrid: gridViewModel'> </div>   <button data-bind='click: addItem'>     Add item </button>   <button data-bind='click: sortByName'>     Sort by name </button>   <button data-bind='click: jumpToFirstPage, enable: gridViewModel.currentPageIndex'>     Jump to first page </button> 
<br>
  </body>
</html>
