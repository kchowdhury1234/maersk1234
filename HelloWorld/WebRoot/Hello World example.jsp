<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
<script type='text/javascript' src='knockout.js'>
  
  </script>
    <base href="<%=basePath%>">
    
    <title>My JSP 'Hello World example.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
   <p>First name: <input data-bind="value: firstName" /></p> <p>Last name: <input data-bind="value: lastName" /></p> <h2>Hello, <span data-bind="text: fullName"> </span>!</h2> 
 <br>
 <script>
 
 // Here's my data model
   var ViewModel = function(first, last){
    this.firstName = ko.observable(first);    
     this.lastName = ko.observable(last); 
          this.fullName = ko.computed(function() {     
              // Knockout tracks dependencies automatically. It knows that fullName depends on firstName and lastName, because these get called when evaluating fullName.  
                     return this.firstName() + " " + this.lastName();   
                       }, this); };
                         ko.applyBindings(new ViewModel("Planet", "Earth")); // This makes Knockout get to work 
  
 
 </script>
  </body>
</html>
